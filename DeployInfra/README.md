# Deploy the infrastructure for WebApp

Deploy the infrastructure with Terraform, from a Gitlab pipeline.

These Terraform files will be used by the GitlabRunner from the Gitlab pipeline to deploy the full infrastructure.
