######
# AZ #
######

data "aws_availability_zones" "available" {
  state = "available"
}

#######
# AMI #
#######

data "aws_ami" "webapp" {
  most_recent      = true
  owners           = ["self"]

  filter {
    name   = "name"
    values = ["WebApp-*"]
  }

  filter {
    name   = "root-device-type"
    values = ["ebs"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}

data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }

  filter {
    name = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}

#################
# NAT USER DATA #
#################

data "local_file" "nat_user_data" {
  filename = "${path.module}/nat_user_data.sh"
}
