FILENAME := $(shell basename $(shell pwd))
TIMESTAMP := $(shell date +%Y-%m-%d_%H%M)
CURRENT_FILE := $(wildcard $(FILENAME)_*.pdf)
ifeq ($(strip $(CURRENT_FILE)),)
override CURRENT_FILE = bogus.pdf
endif

$(CURRENT_FILE): build/main.pdf
	@rm -f $(CURRENT_FILE)
	@cp build/main.pdf $(FILENAME)_$(TIMESTAMP).pdf
	@echo Updated

build/main.pdf: main.tex
	@latexmk -pdf main.tex

clean:
	@rm -rf build 2>/dev/null
