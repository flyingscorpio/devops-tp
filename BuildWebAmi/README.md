# Build an AMI for the WebApp instance

This build should be issued once from within the GitlabRunner instance.

First, install the utils necessary for the build:

```bash
./install-utils.sh
```

This installs Ansible, Packer and Terraform.

Then, build the AMI:

```bash
packer build buildAMI.pkr.hcl
```
