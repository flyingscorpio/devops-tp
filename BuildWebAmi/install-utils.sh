#!/bin/bash

sudo apt install gnupg software-properties-common
curl -fsSL https://apt.releases.hashicorp.com/gpg | sudo apt-key add -
sudo apt-add-repository --update ppa:ansible/ansible
sudo apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main"
sudo apt update
sudo apt install ansible packer terraform

ansible --version
packer -v
terraform -v
