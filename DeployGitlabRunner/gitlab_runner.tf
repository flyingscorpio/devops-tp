#################
# GITLAB RUNNER #
#################

resource "aws_instance" "gitlab_runner" {
  ami = "ami-0b5eea76982371e91"
  instance_type = "t3.micro"

  key_name = "efrei"
  iam_instance_profile = data.aws_iam_instance_profile.labprofile.name
  vpc_security_group_ids = [aws_security_group.allow_ssh.id]

  user_data = base64encode(templatefile("./gitlab_runner_user_data.tftpl", {
    GITLAB_RUNNER_REGISTRATION_TOKEN = var.gitlab_runner_registration_token
  }))

  tags = {
    Name = "GitlabRunner"
  }
}

##################
# SECURITY GROUP #
##################

resource "aws_security_group" "allow_ssh" {
  name        = "allow_ssh"
  description = "Allow SSH inbound traffic"

  ingress {
    description     = "Allow SSH from anywhere"
    from_port       = 22
    to_port         = 22
    protocol        = "tcp"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "allow_ssh"
  }
}
