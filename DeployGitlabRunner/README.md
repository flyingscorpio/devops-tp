# Deploy Gitlab Runner

Locally use Terraform to deploy an EC2 Instance called GitlabRunner.

## Init

It is expected to have AWS credentials stored locally in `~/.aws/credentials`, using the `ini` section `[aws_credentials]`.

Start by initializing terraform with those credentials:

```bash
AWS_PROFILE=aws_credentials terraform init
```

## Apply

To register the gitlab runner, Terraform needs a `gitlab_runner_registration_token` variable.
To avoid leaking it in git, you have to provide the variable through the command line:

```bash
AWS_PROFILE=aws_credentials terraform apply -var gitlab_runner_registration_token="<YOUR_GITLAB_TOKEN>"
```

You can also add the variable in a `terraform.tfvars` file, which is ignored by git.

The runnner should automatically register.
